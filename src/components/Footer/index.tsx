import { Grid, Container, Theme, Box } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';

interface FooterProps {}

const useStyles = makeStyles((theme: Theme) => ({
  platforms: {
    display: 'flex',
  },
  apple: {
    width: 150,
    height: 50,
    backgroundColor: '#ccc',
  },
  google: {
    width: 150,
    height: 50,
    marginRight: 20,
    backgroundColor: '#ccc',
  },
  footer: {
    backgroundColor: '#26262a',
    color: '#fff',
    padding: '16px 0',
  },
}));

export const Footer: React.FC<FooterProps> = () => {
  const classes = useStyles();

  return (
    <Box component="footer" flexShrink="0" className={classes.footer}>
      <Container maxWidth="xl">
        <Grid container alignItems="center" justifyContent="center">
          <Grid item xs={4}>
            <Box
              display="flex"
              justifyContent="space-between"
              alignItems="center"
            >
              <h3>Get the Free App</h3>
              <div className={classes.platforms}>
                <div className={classes.google}></div>
                <div className={classes.apple}></div>
              </div>
            </Box>
          </Grid>

          <Grid item xs={5} style={{ textAlign: 'end' }}>
            &copy;Copyright 2021 Easysnap
          </Grid>
        </Grid>
      </Container>
    </Box>
  );
};
