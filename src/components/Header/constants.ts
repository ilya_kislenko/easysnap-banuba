export const navigationLinks: Array<{ label: string; href: string }> = [
  {
    label: 'Create',
    href: '/create',
  },
  {
    label: 'Products',
    href: '/products',
  },
  {
    label: 'Templates',
    href: '/templates',
  },
  {
    label: 'Pricing',
    href: '/pricing',
  },
];
