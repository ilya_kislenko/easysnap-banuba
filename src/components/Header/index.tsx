import { AppBar, Grid, Button, Link, Container } from '@material-ui/core';
import { Link as RouterLink } from 'react-router-dom';

import { navigationLinks } from './constants';

interface HeaderProps {}

export const Header: React.FC<HeaderProps> = () => {
  return (
    <AppBar position="static">
      <Container maxWidth="xl">
        <Grid container alignItems="center" justifyContent="center">
          <Grid item xs={2}>
            <Link to="/" component={RouterLink}>
              <h1 style={{ fontFamily: 'SFProRounded, sans-serif' }}>
                Easysnap
              </h1>
            </Link>
          </Grid>

          <Grid item xs={5} style={{ display: 'flex' }}>
            {navigationLinks.map(({ label, href }) => (
              <Link
                component={RouterLink}
                style={{ marginRight: '32px' }}
                key={label}
                to={href}
              >
                {label}
              </Link>
            ))}
          </Grid>

          <Grid item xs={2}>
            <Grid container justifyContent="space-between" alignItems="center">
              <Grid item>
                <Button color="primary" variant="text">
                  Log In
                </Button>
              </Grid>
              <Grid item>
                <Button color="primary">Sign Up</Button>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Container>
    </AppBar>
  );
};
