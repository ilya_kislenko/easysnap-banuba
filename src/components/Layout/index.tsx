import { Box } from '@material-ui/core';

import { Footer } from 'components/Footer';
import { Header } from 'components/Header';

interface LayoutContainerProps {}

export const LayoutContainer: React.FC<LayoutContainerProps> = ({
  children,
}) => {
  return (
    <>
      <Box flexGrow="1">
        <Header />
        {children}
      </Box>
      <Footer />
    </>
  );
};
