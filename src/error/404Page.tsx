import { ErrorScreen } from './ErrorScreen';

export const Page404: React.FC = () => {
  return <ErrorScreen title="404 Page" />;
};
