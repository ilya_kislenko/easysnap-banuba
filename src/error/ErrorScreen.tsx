interface ErrorScreenProps {
  title: string;
}

export const ErrorScreen: React.FC<ErrorScreenProps> = ({ title }) => {
  return <h1>{title}</h1>;
};
