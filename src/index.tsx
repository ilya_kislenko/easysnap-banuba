import ReactDOM from 'react-dom';

import App from './modules/App';
import './font/SF-Pro-Rounded-Bold.otf';
import './index.css';

ReactDOM.render(<App />, document.getElementById('root'));
