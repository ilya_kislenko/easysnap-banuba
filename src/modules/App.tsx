import { Route, BrowserRouter as Router, Switch } from 'react-router-dom';
import { NAVIGATION_ROUTES, CustomRoute } from 'routing';
import { MuiThemeProvider, CssBaseline } from '@material-ui/core';
import { createBrowserHistory } from 'history';

import { Page404 } from 'error/404Page';
import { theme } from '../utils/theme';

export const browserHistory = createBrowserHistory();

const App = () => {
  return (
    <MuiThemeProvider theme={theme}>
      <Router>
        <CssBaseline />
        <Switch>
          {NAVIGATION_ROUTES.map((route) => {
            const { component: Cmp, ...otherProps } = route;
            return (
              <CustomRoute
                key={route.path as string}
                {...otherProps}
                render={(props) => <Cmp {...props} />}
              />
            );
          })}
          <Route path="*" component={Page404} />
        </Switch>
      </Router>
    </MuiThemeProvider>
  );
};

export default App;
