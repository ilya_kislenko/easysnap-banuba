import { Grid, Button, Container } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';

interface MainScreenProps {}

const useStyles = makeStyles({
  title: {
    fontFamily: 'Open Sans',
    fontWeight: 800,
    lineHeight: 1.25,
  },
  subTitle: {
    lineHeight: 1.5,
  },
});

export const MainScreen: React.FC<MainScreenProps> = () => {
  const classes = useStyles();
  return (
    <Container maxWidth="xl">
      <Grid container alignItems="center" justifyContent="center">
        <Grid item xs={4}>
          <h1 className={classes.title}>
            Create Amazing Photo & Video Edits With Online Design Tools
          </h1>
          <p className={classes.subTitle}>
            Take your content to the next level with templates, fonts, stickers,
            & more!
          </p>

          <Button color="primary">Start Editing</Button>
        </Grid>
        <Grid item xs={5}>
          <img src="./logo512.png" alt="" />
        </Grid>
      </Grid>
    </Container>
  );
};
