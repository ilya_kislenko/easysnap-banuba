import React from 'react';
import { Route, RouteProps } from 'react-router-dom';

import { LayoutContainer } from 'components/Layout';

export const CustomRoute: React.FC<RouteProps> = ({
  children,
  render,
  ...rest
}) => (
  <LayoutContainer>
    <Route
      {...rest}
      render={(props) => {
        return render?.(props) || children;
      }}
    />
  </LayoutContainer>
);
