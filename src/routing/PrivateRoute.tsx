import { RouteProps, Route } from 'react-router-dom';

export const PrivateRoute: React.FC<RouteProps> = ({
  children,
  render,
  ...rest
}) => {
  return (
    <Route
      {...rest}
      render={(props) => {
        return render?.(props) || children;
      }}
    />
  );
};
