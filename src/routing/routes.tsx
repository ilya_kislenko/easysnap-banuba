import { RouteChildrenProps, RouteProps } from 'react-router-dom';

import { MainScreen } from 'modules/main';

export const NAVIGATION_ROUTES: readonly (RouteProps & {
  component: React.FC<RouteChildrenProps<any>>;
})[] = [
  {
    path: '/',
    exact: true,
    component: MainScreen,
  },
];
