import { createTheme } from '@material-ui/core';
import { PaletteOptions } from '@material-ui/core/styles/createPalette';
import { Overrides } from '@material-ui/core/styles/overrides';
import { ComponentsProps } from '@material-ui/core/styles/props';

const palette: PaletteOptions = {
  primary: {
    main: '#ff7195',
  },
  text: {
    primary: '#26262a',
    secondary: '#8697A8',
  },
  background: {
    default: '#FFFFFF',
  },
  warning: {
    main: '#D4AD47',
  },
};

const overrides: Overrides = {
  MuiButton: {
    root: {
      textTransform: 'none',
      lineHeight: 'normal',
      letterSpacing: 'normal',
      fontWeight: 600,
      '&:hover': {
        backgroundColor: '#FFF',
      },
    },
    outlined: {
      padding: '12px 30px',
      borderRadius: '100px',
      border: 'solid 1px #ff7195',
      '&:hover': {
        backgroundColor: '#FFF',
      },
    },
    outlinedPrimary: {
      border: 'solid 2px #ff7195',
      '&:hover': {
        backgroundColor: '#ff7195',
        border: 'solid 2px #ff7195',
        color: '#fff',
      },
    },
    textPrimary: {
      '&:hover': {
        backgroundColor: 'transparent',
      },
    },
  },
  MuiSelect: {
    select: {
      fontWeight: 500,
    },
  },
  MuiAppBar: {
    colorDefault: {
      backgroundColor: '#fff',
    },
    positionStatic: {
      boxShadow: '0 2px 5px 0 rgba(0, 0, 0, 0.1)',
    },
  },
  MuiLink: {
    root: {
      fontWeight: 600,
      display: 'inline-block',
    },
  },
  MuiPaper: {
    elevation1: {
      boxShadow:
        '0 0 0 1px rgba(63,63,68,0.05), 0 1px 3px 0 rgba(63,63,68,0.15)',
    },
  },
  MuiTab: {
    textColorPrimary: {
      color: palette.text?.primary,
    },
  },
};

const props: ComponentsProps = {
  MuiTextField: {
    variant: 'outlined',
  },
  MuiButton: {
    variant: 'outlined',
    disableElevation: true,
    size: 'medium',
    disableRipple: true,
  },
  MuiAppBar: {
    color: 'default',
  },
  MuiLink: {
    color: 'textPrimary',
    underline: 'none',
  },
};

export const theme = createTheme({
  palette: palette,
  typography: {
    fontFamily: '"Open Sans", "Helvetica", "Arial", sans-serif',
  },
  overrides: overrides,
  props: props,
});
